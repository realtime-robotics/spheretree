#ifndef _VOL_INT_C_
#define _VOL_INT_C_

#include "spheretree/Surface/Surface.h"

namespace stree {

void computeMassProperties(REAL *mass, REAL R[3], REAL J[3][3], const Surface &sur);

} // namespace stree

#endif
