/*************************************************************************\

                             C O P Y R I G H T

  Copyright 2003 Image Synthesis Group, Trinity College Dublin, Ireland.
                        All Rights Reserved.

  Permission to use, copy, modify and distribute this software and its
  documentation for educational, research and non-profit purposes, without
  fee, and without a written agreement is hereby granted, provided that the
  above copyright notice and the following paragraphs appear in all copies.


                             D I S C L A I M E R

  IN NO EVENT SHALL TRININTY COLLEGE DUBLIN BE LIABLE TO ANY PARTY FOR
  DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES, INCLUDING,
  BUT NOT LIMITED TO, LOST PROFITS, ARISING OUT OF THE USE OF THIS SOFTWARE
  AND ITS DOCUMENTATION, EVEN IF TRINITY COLLEGE DUBLIN HAS BEEN ADVISED OF
  THE POSSIBILITY OF SUCH DAMAGES.

  TRINITY COLLEGE DUBLIN DISCLAIM ANY WARRANTIES, INCLUDING, BUT NOT LIMITED
  TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
  PURPOSE.  THE SOFTWARE PROVIDED HEREIN IS ON AN "AS IS" BASIS, AND TRINITY
  COLLEGE DUBLIN HAS NO OBLIGATIONS TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
  ENHANCEMENTS, OR MODIFICATIONS.

  The authors may be contacted at the following e-mail addresses:

          Gareth_Bradshaw@yahoo.co.uk    isg@cs.tcd.ie

  Further information about the ISG and it's project can be found at the ISG
  web site :

          isg.cs.tcd.ie

\**************************************************************************/

#include "spheretree/API/SFWhite.h"
#include "miniball/cpp/main/Seb.h"
#include <vector>
#include <array>

namespace stree {

bool SFWhite::makeSphere(Sphere *s, const Array<Surface::Point> &points, const Array<int> &inds) {
  Array<Point3D> pts;
  convertPoints(&pts, points, inds);
  return makeSphere(s, pts);
}

bool SFWhite::makeSphere(Sphere *s, const Array<Surface::Point> &points) {
  Array<Point3D> pts;
  convertPoints(&pts, points);
  return makeSphere(s, pts);
}

bool SFWhite::makeSphere(Sphere *s, const Array<Point3D> &pts) {
  typedef Seb::Point<REAL> SebPoint;
  typedef Seb::Smallest_enclosing_ball<REAL> Miniball;

  std::vector<SebPoint> seb_points;
  for (int i = 0; i < pts.getSize(); i++) {
    const Point3D p = pts.index(i);
    const std::array<double, 3> data = {p.x, p.y, p.z};
    seb_points.push_back(SebPoint(3, data.data()));
  }
  Miniball ball(3, seb_points);
  s->r = ball.radius();
  s->c.x = ball.center_begin()[0];
  s->c.y = ball.center_begin()[1];
  s->c.z = ball.center_begin()[2];
  return true;
}

} // namespace stree
